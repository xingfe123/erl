#leave these  lines alone
.SUFFIXES: .erl .beam .yrl

erl.beam:
	erlc -W $<
.yrl.erl:
	erlc -W $<
ERL = erl -boot start_clean


#here's a list of the erlang modules you want compiling if the modules
#don't fit onto one line add a \ character to the end of the line and
#continue on the next line
MODS = module module1\

#The first target in any makefile is the default target. if you just type "make" then "make all" is assumed

all:compile

compile:${MODS.%=%.beam} subdirs

#special compilation requirements are added here.
special.beam: special.erl
	${ERL} -Dfalg1 -W0 special.erl

# run an application from the makefile
application: compile
	${ERL} -pa . -s application start arg1 arg2

# the subdirs target compilers any code in sub-directories.
subdirs:
	cd dir; make

#remove all the code. 
clean:
	rm rf *.beam erl_crash.dump
	cd dir; make clean;

