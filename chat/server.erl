server_loop(L)->
    receive
        {mm,Channel,{login, Group,Nick}}->
            case lookup(Group, L )of
                {ok, Pid} ->
                    Pid ! {login, Channel, Nick},
                    server_loop(L);
                error ->
                    Pid = spawn_link(fun()->
                                             start(Channel, Nick)
                                     end),
                    server_loop([{Group,Pid}| L])
            end;
                {mm_closed, _} ->
                    server_loop(L);
                {'EXIT', Pid, allGone} ->
                    L1 = remove_group(Pid, L),
                    server_loop(L1);
                OtherMsg ->
                    io:format("Server received Msg=~p~n",[OtherMsg]),
                    server_loop(L)
    end.
lookup(G, [{G,Pid}|_]) ->    {ok,Pid};
lookup(G, [_ |T]) -> lookup(G,T);
lookup(_,[]) -> [].

group_controller([])->
    exit(allGone);
group_controller(L) ->
    receive
        {C,{relay, Nick, Str}} ->
            foreach(fun({Pid,_})-> Pid ! {msg,Nick,C,Str} end, L),
            group_controller(L);
        {login, C, Nick} ->
            controller(C)
