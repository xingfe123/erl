-module(controller).

start(MM, _, _)->
    process_flag(trap_exit,true),
    io:format("mod chat ~p~n"[MM]),
    loop(MM).
loop(MM)->
    receive
        {chan,MM,Msg}->
            chat_server ! {mm, MM, Msg},
            loop(MM);
        {'EXIT',MM,_Why} ->
            chat_server ! {mm_closed, MM};
        Other ->
            loop(MM)
    end.

