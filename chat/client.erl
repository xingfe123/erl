start()->
    connect("localhost",223,"s","b","c").
connect(Host,Port,Psw,Group,Nick) ->
    spawn(fun()-> 
                  handler(Host,Port,HostPsw,Group,Nick)
          end).

handler(Host, Port, Psw, Group, Nick)->
    process_flag(trap_exit, true),
    Widget = io_widget:start(self()),
    set_title(Widget, Nick),
    set_state(Widget, Nick),
    set_prompt(Widget, [Nick, "> "]),
    set_handler(Widget,fun parse_command/1),
    start_connector(Host, Port, Psw),
    disconnected(Widget, Group, Nick).
start_connector(Host, Port, Psw)->
    S = self(),
    spawn_link(fun()-> try_to_connect(S, Host, Port, Pwd) end).
try_to_connect(Parent, Host, Port, Pwd)->
    case chan:connect(Host, Port, chat, Pwd, []) of
        {error, _Why}->
            Parent ! {status , {cannot, connect, Host, Port}},
            sleep(200),
            try_to_connect(Parent, Host, Port, Pwd);
        {ok, MM} ->
            chan:controller(MM, Parent),
            Parent ! {connected, MM},
            exit(connectorFinished)
    end.
wait_login_reponse(Widget,MM)->
    receive
        {MM, ack}->
            active(Widget,MM);
        Other ->
            io:format("chat client login unexpected:~p~n",[Other]),
            wait_login_reponse(Widget, MM)
    end.
active(Widget, MM)->
    receive
        {Widget, Nick, Str}->
            MM ! {relay, Nick, Str},
            active(Widget, MM);
        {MM, {msg, From, Pid, Str}} ->
            insert_str(Widget, [From,"@", pid_to_list(Pid), " ", Str,"\n"]),
            active(Widget,MM);
        {'EXIT',Widget,WindowDestroyed} ->
            MM ! close;
        {close, MM} ->
            exit(serverDied);
        Other ->
            io:format("chat client active unexpected:~p~n",[Other]),
            active(Widget, MM)
    end.
disconnected(Widget, Group, Nick)->
    receive
        {connected, MM}->
            insert_str(Widget, "Connected to Server\nsending data\n"),
            MM ! {login, Group, Nick},
            wait_login_reponse(Widget,MM);
        {Widget, destoryed} ->
            exit(died);
        {status, S} ->
            insert_str(Widget,to_str(S)),
            disconnected(Widget,Group,Nick);
        Other ->
            io:format("char_client disconnected unexpected:~p~n",[Other]),
            disconnected(Widget, Group, Nick)
    end

